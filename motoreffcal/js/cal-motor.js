'use strict';

window.calMotor = (() => {
    // ขั้นตอนการคำนวน
    // 1.คำนวณช่วง MAX-MIN ในการสุ่ม
    const calStep1 = (inputNameplate, resultBasic, inputPoint1, inputPoint2) => {

        const { Pout, Vr, Ir, PFr, effr, ns, nr, r1, T0 } = inputNameplate;
        const { sr, O, I21Rmin, V21Rmin, FWL, STL } = resultBasic;
        const { Va, Ia, PFa, na } = inputPoint1;
        const { Vb, Ib, PFb, nb, T1 } = inputPoint2;

        const r2min = (Pout * (1 + FWL / 100 + STL / 100) * sr) / (3 * Ir * Ir * (1 - sr))
        const r2max = r2min / (I21Rmin * I21Rmin)

        const rcmin = 3 * (Vr ** 2) * (V21Rmin ** 2) / (0.5 * (Pout) * ((100 / effr) - 1 - FWL / 100 - STL / 100))
        const rcmax = 3 * (Vr ** 2) / (0.1 * (Pout) * ((100 / effr) - 1 - FWL / 100 - STL / 100))

        const Xmmin = (3 * (Vr ** 2) * (V21Rmin ** 2)) / (3 * Vr * Ir * Math.sin(Math.acos(PFr)))
        const Xmmax = (3 * (Vr ** 2)) / (3 * Vr * Ir * Math.sin(Math.acos(PFr)) * 0.2)
        const X1min = (0.2 * 3 * Vr * Ir * Math.sin(Math.acos(PFr))) / (3 * (Ir ** 2) * (1 + 1 / O))
        const X1max = (3 * Vr * Ir * Math.sin(Math.acos(PFr))) / (3 * (Ir ** 2) * (1 + (I21Rmin * I21Rmin) / O))
        const X2min = X1min / O
        const X2max = X1max / O

        return {
            sr,
            r1,
            r2min,
            r2max,
            rcmin,
            rcmax,
            Xmmin,
            Xmmax,
            X1min,
            X1max,
        };
    }

    // 2.หาค่า Z และ I PF Power จากการคำนวณ(Zin,Ic,PFc,Pc)
    const calStep2 = (inputNameplate, resultBasic, inputPoint1, inputPoint2) => {

        const { Pout, Vr, Ir, PFr, effr, ns, nr, r1, T0 } = inputNameplate
        const { sr, O, I21Rmin, V21Rmin, FWL, STL } = resultBasic;
        const { Va, Ia, PFa, na } = inputPoint1;
        const { Vb, Ib, PFb, nb, T1 } = inputPoint2;

        //  input for loop calculation Point A
        const r1A = r1
        const r2A = 0.072
        const rcA = 420.4669
        const xmA = 9.5622
        const x1A = 0.101
        const x2A = x1A / O

        // find ZA 
        const sA = (ns - na) / ns
        const Pa = 3 * Va * Ia * PFa
        const NRa = (rcA * xmA * r2A / sA) * (rcA * x2A + rcA * xmA + xmA * (r2A / sA)) - rcA * x2A * xmA * (rcA * (r2A / sA) - x2A * xmA)
        const NIa = rcA * xmA * (r2A / sA) * (rcA * (r2A / sA) - x2A * xmA) + rcA * x2A * xmA * (rcA * x2A + rcA * xmA + xmA * (r2A / sA))
        const Da = ((rcA * (r2A / sA) - x2A * xmA) ** 2) + ((rcA * x2A + rcA * xmA + xmA * (r2A / sA)) ** 2)
        const ZinRA = r1A + NRa / Da
        const ZinIA = x1A + NIa / Da
        const ZinA = Math.sqrt((ZinRA * ZinRA + ZinIA * ZinIA))

        // find Ic PFc Pc for Point A
        const IcA = Va / ZinA
        const PFcA = ZinRA / ZinA
        const PcA = 3 * Va * IcA * PFcA

        // input for loop calculation Point B
        const r1B = r1
        const r2B = 0.072
        const rcB = 420.4669
        const xmB = 9.5622
        const x1B = 0.101
        const x2B = x1B / O

        // find ZB
        const sB = (ns - nb) / ns
        const Pb = 3 * Vb * Ib * PFb
        const NRb = (rcB * xmB * r2B / sB) * (rcB * x2B + rcB * xmB + xmB * (r2B / sB)) - rcB * x2B * xmB * (rcB * (r2B / sB) - x2B * xmB)
        const NIb = rcB * xmB * (r2B / sB) * (rcB * (r2B / sB) - x2B * xmB) + rcB * x2B * xmB * (rcB * x2B + rcB * xmB + xmB * (r2B / sB))
        const Db = ((rcB * (r2B / sB) - x2B * xmB) ** 2) + ((rcB * x2B + rcB * xmB + xmB * (r2B / sB)) ** 2)
        const ZinRB = r1B + NRb / Db
        const ZinIB = x1B + NIb / Db
        const ZinB = Math.sqrt((ZinRB * ZinRB + ZinIB * ZinIB))

        // find Ic PFc Pc for Point B
        const IcB = Vb / ZinB
        const PFcB = ZinRB / ZinB
        const PcB = 3 * Vb * IcB * PFcB

        return {
            r1A,
            r2A,
            rcA,
            xmA,
            x1A,
            x2A,
            sA,
            Pa,
            NRa,
            NIa,
            Da,
            ZinRA,
            ZinIA,
            ZinA,
            IcA,
            PFcA,
            PcA,
            r1B,
            r2B,
            rcB,
            xmB,
            x1B,
            x2B,
            sB,
            Pb,
            NRb,
            NIb,
            Db,
            ZinRB,
            ZinIB,
            ZinB,
            IcB,
            PFcB,
            PcB,
        }
    }

    const calStep3 = (inputNameplate, resultBasic, inputPoint1, inputPoint2, resultStep2) => {
        const { Pout, Vr, Ir, PFr, effr, ns, nr, r1, T0 } = inputNameplate
        const { sr, O, I21Rmin, V21Rmin, FWL, STL } = resultBasic;
        const { Va, Ia, PFa, na } = inputPoint1;
        const { Vb, Ib, PFb, nb, T1 } = inputPoint2;
        const { IcA, PFcA, PcA, Pa, IcB, PFcB, PcB, Pb } = resultStep2;
        const errA = 100 * Math.sqrt(((((IcA - Ia) / Ia) ** 2) + (((PFcA - PFa) / PFa) ** 2) + (((PcA - Pa) / Pa) ** 2)) / 3)
        const errB = 100 * Math.sqrt(((((IcB - Ib) / Ib) ** 2) + (((PFcB - PFb) / PFb) ** 2) + (((PcB - Pb) / Pb) ** 2)) / 3)
        const errT = Math.sqrt(((errA ** 2) + (errB ** 2)) / 2)

        return {
            errA,
            errB,
            errT,
        }
    }

    // 4.คำนวนค่าประสิทธิภาพมอเตอร์
    const calStep4 = (inputNameplate, resultBasic, inputPoint1, inputPoint2, resultStep2) => {
        const { Pout, Vr, Ir, PFr, effr, ns, nr, r1, T0 } = inputNameplate
        const { sr, O, I21Rmin, V21Rmin, FWL, STL } = resultBasic;
        const { Va, Ia, PFa, na } = inputPoint1;
        const { Vb, Ib, PFb, nb, T1 } = inputPoint2;
        const {
            r1A,
            r2A,
            rcA,
            xmA,
            x1A,
            x2A,
            sA,
            Pa,
            NRa,
            NIa,
            Da,
            ZinRA,
            ZinIA,
            ZinA,
            IcA,
            PFcA,
            PcA,
            r1B,
            r2B,
            rcB,
            xmB,
            x1B,
            x2B,
            sB,
            Pb,
            NRb,
            NIb,
            Db,
            ZinRB,
            ZinIB,
            ZinB,
            IcB,
            PFcB,
            PcB,
        } = resultStep2;

        // Point A
        const i1RA = IcA * ZinRA / ZinA
        const i1IA = -1 * IcA * ZinIA / ZinA
        const v2A2 = ((Va - (r1A * i1RA - x1A * i1IA)) ** 2) + ((-1 * (r1A * i1IA + x1A * i1RA)) ** 2)
        const i2A2 = v2A2 / (((r2A / sA) ** 2) + (x2A ** 2))
        const effa = 100 * i2A2 * r2A * (1 - sA) / (sA * (1 + (FWL / 100) + STL * i2A2 / (87.75 * Ir * Ir)) * (Va * IcA * PFcA))

        // Point B
        const i1RB = IcB * ZinRB / ZinB
        const i1IB = -1 * IcB * ZinIB / ZinB
        const v2B2 = ((Vb - (r1B * i1RB - x1B * i1IB)) ** 2) + ((-1 * (r1B * i1IB + x1B * i1RB)) ** 2)
        const i2B2 = v2B2 / (((r2B / sB) ** 2) + (x2B ** 2))
        const effb = 100 * i2B2 * r2B * (1 - sB) / (sB * (1 + (FWL / 100) + STL * i2B2 / (87.75 * Ir * Ir)) * (Vb * IcB * PFcB))

        return {
            effa,
            effb,
        };
    }

    const calEff = (inputNameplate, inputPoint1, inputPoint2) => {
        const resultBasic = calBasic(inputNameplate);
        const resultStep1 = calStep1(inputNameplate, resultBasic, inputPoint1, inputPoint2);
        const resultStep2 = calStep2(inputNameplate, resultBasic, inputPoint1, inputPoint2);
        const resultStep3 = calStep3(inputNameplate, resultBasic, inputPoint1, inputPoint2, resultStep2);
        const resultStep4 = calStep4(inputNameplate, resultBasic, inputPoint1, inputPoint2, resultStep2);

        const { errA, errT } = resultStep3;
        const { effa, effb } = resultStep4;

        return { errA, errT, effa, effb };
    }

    return { calEff };
})();
