<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Motor Eff</title>
<link href="css/bootstrap-3.3.7.css" rel="stylesheet" type="text/css">
</head>

<body class="bg-colorgreen">
<!-- Navigation Head Menu -->
<?php include ("header.php"); ?>
<!--check textbox กรอกเฉพาะตัวเลข-->
<SCRIPT language=JavaScript>
function check_number() {
e_k=event.keyCode
//if (((e_k < 48) || (e_k > 57)) && e_k != 46 ) {
if (e_k != 13 && (e_k < 46) || (e_k > 57)) {
event.returnValue = false;
alert("ต้องเป็นตัวเลขเท่านั้น... \nกรุณาตรวจสอบข้อมูลของท่านอีกครั้ง...");
}
}
</script>
<!--check textbox กรอกเฉพาะตัวเลข-->
<div class="container bg-colorwhite">
	<h3>ทดสอบประสิทธิภาพมอเตอร์</h3>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<div class="form-row">
		<div class="form-group col-md-12">
          	<h4>รายการมอเตอร์ที่จะทดสอบ : ลำดับที่ ...ยี่ห้อ.... รุ่น.....</h4>
		</div>
		<div class="form-group col-md-12">
		<h3>จุดที่ 1</h3>
		 <div class="form-group col-md-3">
			<label>Rated Voltage (Va)</label>
			<input type="text" name="Va" class="form-control" placeholder="Rated Voltage (Va)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Current (Ia)</label>
			<input type="text" name="Ia" class="form-control" placeholder="Rated Current (Ia)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Power (PFa)</label>
			<input type="text" name="PFa" class="form-control" placeholder="Rated Power (PFa)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Speed (Na)</label>
		 	<input type="text" name="Na" class="form-control" placeholder="Rated Speed (Na)" onkeypress=check_number();>
		</div>
		
		<h3>จุดที่ 2</h3>
		 <div class="form-group col-md-3">
			<label>Rated Voltage (Vb)</label>
			<input type="text" name="Vb" class="form-control" placeholder="Rated Voltage (Vb)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Current (Ib)</label>
			<input type="text" name="Ib" class="form-control" placeholder="Rated Current (Ib)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Power (PFb)</label>
			<input type="text" name="PFb" class="form-control" placeholder="Rated Power (PFb)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Speed (Nb)</label>
		 	<input type="text" name="Nb" class="form-control" placeholder="Rated Speed (Nb)" onkeypress=check_number();>
		</div>
		<hr>
		<div class="form-group col-md-3">
			<label>ค่าความต้านทานขดลวดสเตเตอร์ (R1)</label>
		 	<input type="text" name="R1" class="form-control" placeholder="R1">
		</div>
		<div class="form-group col-md-3">
			<label>อุณหภูมิขดลวดขณะวัด (T1)</label>
		 	<input type="text" name="T1" class="form-control" placeholder="T1">
		</div>
		</div>
		<div class="text-center">
		<input class="btn btn-primary" type="submit" name="sent" value="ทดสอบ" onclick="dosubmit()">
		<input class="btn btn-outline-primary" type="reset" name ="cancle" value="reset">
		</div>
	</form>
	<br>

</div>
</div>
<!-- InstanceEndEditable -->
<footer class="footer mt-auto py-3 text-center">
  <div class="container">
	<span class="text-muted">Copyright © 2019 โครงการส่งเสริมการใช้งานโปรแกรมประเมินประสิทธิภาพมอเตอร์ไฟฟ้า. All Rights Reserved.</span>
  </div>
</footer>

<!--<script src="../js/jquery-1.11.3.min.js"></script> -->
<script src="js/bootstrap.js"></script>
</body>
<!-- InstanceEnd -->
</html>
