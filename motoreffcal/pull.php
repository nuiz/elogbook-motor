<?php

$link = mysqli_connect("103.91.189.243", "admindb", "E1ogbook123", "elogbook202003281513hxf", 63307) or die("Unable to Connect to 'elogbook202003281513hxf'");

$dataRaw = file_get_contents("http://103.91.189.148/elogbook/data-202003281513Hxf.json");
$data = json_decode($dataRaw, true);
$tgroups = $data["tgroup"];

class ReduceSensorProcess {
    private $deviceId;
    private $mappingSensorAttr;

    public function __construct($deviceId) {
        $this->deviceId = $deviceId;
        $this->mappingSensorAttr = array(
            "V_MEA1_MO#" => "Va",
            "I_MEA1_MO#" => "Ia",
            "P_MEA1_MO#" => "Pa",
            "N_MEA1_MO#" => "na",
            "T_MEA1_MO#" => "t0",
            "V_MEA2_MO#" => "vb",
            "I_MEA2_MO#" => "Ib",
            "P_MEA2_MO#" => "Pb",
            "N_MEA2_MO#" => "nb",
            "T_MEA2_MO#" => "t1",
            "OHM_STATOR_MO#" => "r1",
        );
    }

    public function reduceSensorAttr($rows) {		
        $acc = array();
        foreach ($rows as $key => $item) {
            $key = $item["flabel"];
            $value = $item["fvalue"];
            $convertedValue = $value * 1;
            $acc[$this->mappingSensorAttr[$key]] = $convertedValue;
        }
        
        return $acc;
    }
}

class MyCallback {

    private $link;

    public function __construct($link) {
        $this->link = $link;
    }

    private $mappingDeviceAttr = array(
        "ID" => "id",
        "BRAND_MO#" => "fac",
        "MODEL_MO#" => "MN",
        "P_RATE_MO#" => "Pout",
        "V_RATE_MO#" => "Vr",
        "I_RATE_MO#" => "Ir",
        "PF_RATE_MO#" => "PFr",
        "EFF_SPEC_MO#" => "effr",
        "RPM_SYN_MO#" => "ns",
        "RPM_RATE_MO#" => "nr"
    );

    public function reduceDeviceAttr($rows) {
        $acc = array();
        foreach ($rows as $key => $item) {
            if (!array_key_exists("fconfig", $item)) {
                continue;
            }
            $fconfig = $item["fconfig"];
            if (empty($fconfig)) {
                continue;
            }
            if (!array_key_exists($fconfig, $this->mappingDeviceAttr)) {
                continue;
            }
            $convertedValue = is_numeric($item["fvalue"]) ? $item["fvalue"] * 1 : $item["fvalue"];
            if ($convertedValue == "") {
                continue;
            }
            $convertedKey = $this->mappingDeviceAttr[$fconfig];
            $acc[$convertedKey] = $convertedValue;
        }

        return $acc;
    }

    public function getSensorData($deviceId) {
        $query = <<<QUERY
        SELECT t1.* from(
            SELECT flabel,fvalue FROM tsensor_minute where flabel like '%MO${deviceId}' order by fdatetime desc
        ) t1 group by t1.flabel;
QUERY;
        $result = $this->link->query($query);
        $rows = $this->fetchAll($result);
        $reduceProcerss = new ReduceSensorProcess($deviceId);
        return $reduceProcerss->reduceSensorAttr($rows);
    }

    private function fetchAll($result) {
        $rows = array();
        while($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }
}

$myCallback = new MyCallback($link);

function calEff($content) {
    $ch = curl_init("http://asia-east2-nuiz-165310.cloudfunctions.net/motoreff_calv2");
    $payload = json_encode($content);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    $result = curl_exec($ch);
    curl_close($ch);

    return json_decode($result, true);
}

$devices = array();

foreach($tgroups as $keyGroup => $tgroup) {
    $tdevices = $tgroup[tdevice];
    foreach($tdevices as $keyDevice => $tdevice) {
        $device = $myCallback->reduceDeviceAttr($tdevice[tscript]);
        $sensor = $myCallback->getSensorData($device[id]);
        $content = array_merge($device, $sensor);
        try {
            $deviceId = $content[id];
            echo "calculate device id: $deviceId\n";

            $calResult = calEff($content);
            if (gettype($calResult) != "array") {
                $type = gettype($calResult);
                echo "type: $type\n";
                throw new Exception($calResult);
            }
            $contentToLog = array(
                "__company__" => "202003281513hxf",
            );
            if (array_key_exists("effa", $calResult)) {
				$k = "EFF_A_MO".$content[id];
                $contentToLog[$k] = $calResult[effa];
            }
            if (array_key_exists("effb", $calResult)) {
				$k = "EFF_B_MO".$content[id];
                $contentToLog[$k] = $calResult[effb];
            }

            $logUrl = "http://103.91.189.148/elogbook/log.php?".http_build_query($contentToLog);
            file_get_contents($logUrl);

            // $jsonContent = json_encode($content);
            // $jsonCalResult = json_encode($calResult);
            // echo "calculate success content: $jsonContent\n";
            // echo "calculate success calResult: $jsonCalResult\n";
        }
        catch (Exception $ex) {
            // $jsonContent = json_encode($content);
            // echo "calculate error exception: $ex\n";
            // echo "calculate error content: $jsonContent\n";
        }

        echo "=========\n";
    }
}
