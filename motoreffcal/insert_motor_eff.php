<!doctype html>
<html><!-- InstanceBegin template="/Templates/template.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Motor Eff</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--<link href="../css/sticky-footer.css" rel="stylesheet" type="text/css">-->
<link href="css/bootstrap-3.3.7.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Bootstrap core CSS -->
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

	.result-box {
		background: white;
		padding: 10px;
		color: black;
		font-size: 22px;
		border: 1px solid #8dc733;
	}

	.result-box.small {
		font-size: 12px;
	}

	.nonevisible {
		/* visibility: hidden; */
		display: none;
	}

	.box1 {
		padding: 20px;
		padding-top: 16px;
		border: 1px solid #958eff;
		background: #327ab7;
		color: white;
		font-weight: bold;
	}

	.padding-a {
		padding-top: 35px;
		padding-bottom: 35px;
	}

	#result-description {
		border: 1px dashed black;
	}

	.go-result-description-btn-block {
		margin-top: 8px;
	}
	.go-result-description-btn {
		color: white;
    	text-decoration: underline;
	}

	.description-list {
		margin-left: 172px;
	}

	.color-red {
		color: red;
	}
	</style>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172762585-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-172762585-1');
	</script>
</head>

<body background="images/grid.png">
<!-- Navigation Head Menu -->

<!-- InstanceBeginEditable name="EditRegion3" -->
<!--check textbox กรอกเฉพาะตัวเลข-->
<SCRIPT language=JavaScript>
function check_number() {
e_k=event.keyCode
//if (((e_k < 48) || (e_k > 57)) && e_k != 46 ) {
if (e_k != 13 && (e_k < 46) || (e_k > 57)) {
event.returnValue = false;
alert("ต้องเป็นตัวเลขเท่านั้น... \nกรุณาตรวจสอบข้อมูลของท่านอีกครั้ง...");
}
}
</script>
<!--check textbox กรอกเฉพาะตัวเลข-->

<!--ปฏิทินกรอกวันที่-->
<script src="js/jquery-1.6.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
<script src="js/cal-motor.js" type="text/javascript"></script>
<link href="js/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document) .ready(function() {
		$("#date_s") .datepicker({
			dateFormat : "yy-mm-dd"
		});
		$("#b_maturity_date") .datepicker({
			dateFormat : "yy-mm-dd"
		});
		$("#t_maturity_date") .datepicker({
			dateFormat : "yy-mm-dd"
		});
		$("#o_maturity_date") .datepicker({
			dateFormat : "yy-mm-dd"
		});
	});
</script>
<!--ปฏิทินกรอกวันที่-->

<div class="container">
	<h3>ทดสอบประสิทธิภาพมอเตอร์ </h3>
	<form id="cal-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<div class="form-row">
		<div class="form-group col-md-6">
			<label>ยี่ห้อ</label>
		 	<input type="text" name="brand" class="form-control" placeholder="Brand">
		</div> 
		<div class="form-group col-md-6">
			<label>รุ่น</label>
		 	<input type="text" name="model" class="form-control" placeholder="Model">
		 </div>
		 <div class="form-group col-md-3">
			<label>Rated Power (KW)*</label>
			<input type="text" name="Pout" class="form-control" placeholder="Rated Power (KW)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Voltage (V)*</label>
		 	<input type="text" name="Vr" class="form-control" placeholder="Rated Voltage (V)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Current (A)*</label>
			<input type="text" name="Ir" class="form-control" placeholder="Rated Current (A)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Power Factor</label>
			<input type="text" name="PFr" class="form-control" placeholder="Power Factor" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Rated Efficiency (%)*</label>
		 	<input type="text" name="effr" class="form-control" placeholder="Rated Efficiency (%)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Synchronous speed (ns)*</label>
		 	<input type="text" name="ns" class="form-control" placeholder="Synchronous speed (ns)*">
		</div>
		<div class="form-group col-md-3">
			<label>Rated Speed (rpm)*</label>
		 	<input type="text" name="nr" class="form-control" placeholder="Rated Speed (rpm)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Stator resistance per phase (Ω)* </label>
		 	<input type="text" name="r1" class="form-control" placeholder="Stator resistance per phase (Ω)* " onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Stator temp during  R measurement (oC)*</label>
		 	<input type="text" name="t0" class="form-control" placeholder="Stator temp during  R measurement (oC)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Stator temp during operation (oC)</label>
		 	<input type="text" name="t1" class="form-control" placeholder="Stator temp during operation (oC)" onkeypress=check_number();>
		</div>
		</div>

		<div class="form-group col-md-12 bg-colorgreen">
		<h3>ค่าตรวจวัด ภาระโหลดที่ 1</h3>
		 <div class="form-group col-md-3">
			<label>Volt p (V1)*</label>
			<input type="text" name="Va" class="form-control" placeholder="Volt p (V1)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Current (A1)*</label>
			<input type="text" name="Ia" class="form-control" placeholder="Current (A1)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Power(kW1)*</label>
			<input type="text" name="Pa" class="form-control" placeholder="Power(kW1)*" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Speed(rpm1)*</label>
		 	<input type="text" name="na" class="form-control" placeholder="Speed(rpm1)*" onkeypress=check_number();>
		</div>
		
		<h3>ค่าตรวจวัด ภาระโหลดที่ 2 (ถ้ามี)</h3>
		 <div class="form-group col-md-3">
			<label>Volt p (V2)</label>
			<input type="text" name="Vb" class="form-control" placeholder="Volt p (V2)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Current (A2)</label>
			<input type="text" name="Ib" class="form-control" placeholder="Rated Current (Ib)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Power(kW2)</label>
			<input type="text" name="Pb" class="form-control" placeholder="Power(kW2)" onkeypress=check_number();>
		</div>
		<div class="form-group col-md-3">
			<label>Speed(rpm2)</label>
		 	<input type="text" name="nb" class="form-control" placeholder="Speed(rpm2)" onkeypress=check_number();>
		</div>
		</div>
		
		<div class="text-center col-md-12">
		<!-- <input class="btn btn-primary" type="button" name="put-sample-data-btn-1" value="ใส่ข้อมูลตัวอย่าง1จุด"> -->
		<!-- <input class="btn btn-primary" type="button" name="put-sample-data-btn-2" value="ใส่ข้อมูลตัวอย่าง2จุด"> -->
		<input class="btn btn-success" type="submit" name="sent" value="คำนวนผล">
		<input class="btn btn-outline-primary" type="reset" name="cancle" value="Reset">
		</div>
	</form>

	<div id="output-print" class="text-center col-md-12" style="margin-top: 20px;"></div>
	<div class="row">
		<div class="col-md-12 text-center" id="result-description">
			<h3>คำอธิบายผล</h3>
			<p>
				ค่าประสิทธิภาพมอเตอร์  (%)   ค่าประสิทธิภาพ ณ <span class="color-red">ภาระการทำงานที่ 1</span> คำนวณจากวงจรสมมูลย์ของมอเตอร์ และค่าตรวจวัดที่ป้อนให้โปรแกรม
			</p>
			<table class="table text-left">
				<tr>
					<td width="200">Calculation Mode</td>
					<td>
						<p>โปรแกรมวิเคราะห์ค่าประสิทธิภาพมอเตอร์ ด้วยวิธีวงจรสมมูลย์ และ หาคำตอบด้วย</p>
						<p>
							DIFFERENTIAL EVOLUTION OPTIMIZATION (DEO)  ในการคำนวณรอบแรก โปรแกรมจะใช้ข้อมูลทั้ง 2 ภาระการทำงาน หรือ “TWO POINTS” ในการหาคำตอบ ซึ่งจะได้คำตอบที่แม่นยำ แต่หากคำตอบที่ได้คลาดเคลื่อน โปรแกรมจะใช้ข้อมูล ภาระการทำงานแรก เพียงชุดเดียวในการหาคำตอบ หรือ “SINGLE POINT” ความคลาดเคลื่อนมีโอกาสสูงขึ้น เนื่องจากเป็นการหาคำตอบที่ทำให้จุดทำงานแรกเป็นจริงจุดเดียวก็เพียงพอ
						</p>
					</td>
				</tr>
				<tr>
					<td>Solution Error  (%)</td>
					<td>
						ได้แก่ ค่า err1 และ errT เป็นค่าที่บอกความคลาดเคลื่อนในการหาคำตอบ (ไม่ใช่ความคลาดเคลื่อนของค่าประสิทธิภาพที่ประเมินได้กับค่าประสิทธิภาพจริง)  เนื่องจากโปรแกรมจะหาวงจรย์สมมูลย์ที่มี กระแส ความเร็วรอบ และกำลังไฟฟ้าใกล้เคียงกับค่าตรวจวัดมากที่สุด หากได้ค่าเท่าค่าตรวจวัดพอดี  Solution Error จะเป็น 0.00 % และ ยิ่งแตกต่างมากค่าร้อยละความแตกต่างก็จะสูงขึ้น โดยที่ err1 เป็น Solution error ของการหาคำตอบภาระการทำงานจุดที่ 1 และ errT เป็น Solution error ของการหาคำตอบทั้งสองจุดทำงาน และหาก errT เกินกว่า 1% โปรแกรมจะเปลี่ยนการคำนวณแบบ TWO Points เป็น Single Point
					</td>
				</tr>
				<tr>
					<td>ความแม่นยำของการคำนวณ</td>
					<td>
						ความแม่นยำของโปรแกรมมี 2 ระดับ คือ แบบ TWO POINTS  โปรกรมจะหาคำตอบด้วยข้อมูลตรวจวัดทั้งสองภาระการทำงาน  แบบนี้ความแม่นยำจะสูง แต่หากการใช้ข้อมูลสองจุดทำงานไม่สามารถหาคำตอบได้ หรือมี Solution Error เกินกว่า 1% โปรแกรมจะคำนวณอีกครั้งโดยใช้ข้อมูลที่<span class="color-red">ภาระการทำงานที่ 1</span> จุดเดียว หรือ SINGLE POINT แบบนี้ความคลาดเคลื่อนจะสุงขึ้น เนื่องจากใช้ข้อมูลการทำงานชุดเดียว ในทางปฎิบัติควรตรวจวัดซ้ำ และป้อนข้อมูลชุดใหม่
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p>ความไม่แม่นยำของการประเมินเกิดจากสาเหตุใดบ้าง</p>
						<p>หากโปรแกรมไม่สามารถหาคำตอบจาก<span class="color-red">ภาระการทำงาน</span> 2 จุดได้  หรือ Solution Error มีค่าสูง อาจเนื่องจาก</p>
						<ul class="description-list">
							<li>มอเตอร์ขนาดเล็ก มอเตอร์มีขนาดเล็กกว่า 1 กิโลวัตต์ มีแนวโน้มที่จะมีความคลาดเคลื่อนสูง</li>
							<li>มอเตอร์มีภาระต่ำ มอเตอร์ควรมีภาระมากกว่า 60 %</li>
							<li>จุดทำงานทั้งสองที่ตรวจวัด มีภาระต่างกันมาก</li>
							<li>มอเตอร์เป็นชนิดพิเศษต่างจากรุ่นทั่วไป</li>
							<li>ค่าที่ตรวจวัด หรือค่าพิกัดที่ป้อนผิดพลาด</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p>การแก้ไข หากค่าความคลาดเคลื่อนสูง หรือโปรแกรมใช้โหมด SINGLE POINT แต่ผู้ใช้ต้องการผลที่แม่นยำมากยิ่งขึ้น</p>
						<ul class="description-list">
							<li>ควรตรวจสอบค่าที่ตรวจวัดว่าถูกต้อง หรือตรวจวัดค่าชุดใหม่ป้อนให้โปรแกรมอีกครั้ง</li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- InstanceEndEditable -->
	<footer class="footer mt-auto py-3">
  <div class="container">
	<p class="text-center">กลุ่มวิจัยเพื่อการอนุรักษ์พลังงาน มหาวิทยาลัยเททคโนโลยีพระจอมเกล้าธนบุรี   โทร 0 2470 9604-8 ต่อ 1201 อีเมล์ enconlab@kmutt.ac.th</p>
	<p class="text-center">
		<span class="text-muted">โปรแกรมทดสอบประสิทธิภาพมอเตอร์ไฟฟ้า version 1.20</span>
	</p>
  </div>
</footer>

<!--<script src="../js/jquery-1.11.3.min.js"></script> -->
<script src="js/bootstrap.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
	$(() => {
		const samlpleData1 = {
			"fac": "001",
			"MN": "1ABB2.2",
			"brand": "001",
			"model": "1ABB2.2",
			"Pout": 370,
			"Vr": 265.58,
			"Ir": 0.85,
			"PFr": 0.72,
			"effr": 78.2,
			"ns": 1800,
			"nr": 1756,
			"Va": 265.01,
			"Ia": 0.81,
			"Pa": 463.66,
			"na": 1767,
			"t0": 24,
			"t1": 95,
			"r1": 14.7375
		};

		const samlpleData2 = {
			"fac": "001",
			"MN": "1ABB2.2",
			"brand": "001",
			"model": "1ABB2.2",
			"Pout": 2.200,
			"Vr": 400,
			"Ir": 4.56,
			"PFr": 0.82,
			"effr": 85.9,
			"ns": 3000,
			"nr": 2887,
			"r1": 1.9628,
			"t0": 20,
			"t1": 67,
			"Va": 231,
			"Ia": 4.45,
			"Pa": 2.589,
			"na": 2882,
			"Vb": 231,
			"Ib": 3.58,
			"Pb": 1.935,
			"nb": 2914
		};
		
		const $outputEl = $('#output-print');
		const $calForm = $('#cal-form');
		const $putSampleDataBtn1 = $('input[name="put-sample-data-btn-1"]');
		const $putSampleDataBtn2 = $('input[name="put-sample-data-btn-2"]');

		$putSampleDataBtn1.click((event) => {
			event.preventDefault();
			Object.keys(samlpleData1).forEach((el) => {
				const $el = $('[name="'+ el +'"]', $calForm);
				$el.val(samlpleData1[el]);
			});
			["Vb", "Ib", "Pb", "nb"].forEach((el) => {
				const $el = $('[name="'+ el +'"]', $calForm);
				$el.val('');
			});
		});

		$putSampleDataBtn2.click((event) => {
			event.preventDefault();
			Object.keys(samlpleData2).forEach((el) => {
				const $el = $('[name="'+ el +'"]', $calForm);
				$el.val(samlpleData2[el]);
			});
		});

		let currentResolve = null;
		let dotInterval = null;
		$calForm.submit((event) => new Promise((resolve, reject) => {
			event.preventDefault();
			const formSerialized = $(event.target).serializeArray();
			const basicNames = ['Pout', 'Vr', 'Ir', 'PFr', 'effr', 'ns', 'nr', 'r1', 't0', 't1', 'Va', 'Ia', 'Pa', 'na'];
			const bNames = ['Vb', 'Ib', 'Pb', 'nb'];
			const bExists = formSerialized.some(el => bNames.includes(el.name) && el.value != '');
			const names = bExists ? [...basicNames, ...bNames] : basicNames;
			const formSerializedFilterd = names.map(el => {
				const elFound = formSerialized.find(el2 => el2.name == el);
				if (elFound == undefined) {
					alert("บางอย่าไม่ถูกต้อง กรุณาติดต่อผู้ดูแลระบบ...");
					console.warn(`elFound == undefined key: ${el}`);
					throw Error(`elFound == undefined key: ${el}`);
				}
				if (elFound.value == '') {
					alert("กรอกข้อไม่ถูกต้อง... \nกรุณาตรวจสอบข้อมูลของท่านอีกครั้ง...\nหากต้องการคำนวนจุด A จุดเดียวไม่ต้องใส่ข้อมูลจุด B");
					console.warn(`missing input ${el}: ${elFound.value}`);
					throw Error(`missing input ${el}: ${elFound.value}`);
				}
				return elFound;
			});
			const dataToSend = formSerializedFilterd.reduce((acc, el) => {
				acc[el.name] = el.value * 1;
				return acc;
			}, {
				"fac": "001",
				"MN": "1ABB2.2",
			});

			$outputEl.text('กำลังประมวลผล');
			clearInterval(dotInterval);
			dotInterval = setInterval(() => {
				$outputEl.text($outputEl.text() + '.');
			}, 1000);
			const internalResolve = (data) => {
				const effA = data.effa != undefined ? data.effa.toFixed(2) + '%' : '';
				const effB = data.effb != undefined ? data.effb.toFixed(2) + '%' : '';
				const errA = data.errA != undefined ? data.errA.toFixed(2) + '%' : '';
				const errT = data.errT != undefined ? data.errT.toFixed(2) + '%' : '-';
				const elHtml = `<div class="form-group col-md-8 col-md-offset-2 box1">
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-12">
								<div>Efficiency A</div>
								<div class="result-box ${data.effb ? '' : 'padding-a'}">${effA}</div>
							</div>
							<div class="col-md-12 ${data.effb ? '' : 'nonevisible'}">
								<div>Efficiency B</div>
								<div class="result-box">${effB}</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12">
								<div>Calculation Mode</div>
								<div class="result-box">${data.effb ? 'TWO POINTS' : 'SINGLE POINT'}</div>
							</div>
							<div class="col-md-12">
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-6">
										<div class="col-md-4" style="padding-top: 10px; padding-left: 0;">err1 </div>
										<div class="col-md-8 result-box small">${errA}</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-4" style="padding-top: 10px; padding-left: 0;">errT </div>
										<div class="col-md-8 result-box small">${errT}</div>
									</div>
								</div>
								<div class="col-md-12 text-center go-result-description-btn-block">
									<a class="go-result-description-btn" href="">คำอธิบายผล</a>
								</div>
							</div>
						</div>
					</div>
				</div>`;
				const elsHtmlWraped = `<div>${elHtml}</div>`;
				$outputEl.html(elsHtmlWraped);
				$('.go-result-description-btn', $outputEl).click((e) => {
					e.preventDefault();
					$('html, body').animate({
						scrollTop: $('#result-description').offset().top
					}, 300);
				});
				clearInterval(dotInterval);
			};
			currentResolve = internalResolve;
			axios.post('http://103.91.189.148:8010/cal-web', dataToSend)
				.then(res => res.data)
				.then((data) => internalResolve === currentResolve && internalResolve(data));
		}).catch(() => {
			clearInterval(dotInterval);
			currentResolve = null;
			$outputEl.html('');
		}))
	});
</script>
</body>
<!-- InstanceEnd --></html>
