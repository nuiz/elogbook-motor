import math

#ข้อมูลพื้นฐานของมอเตอร์ NAMEPLATE DATA
Pout = 45000 # Rated Power Watt
Vr = 220  # Phase Voltage Volt
Ir = 86  # Phase Current Am
PFr = 0.87  # Power Factor
effr = 91.5  # efficiency
ns = 1500 # Ns rpm
nr = 1450  # Rated rpm rpm

#ค่าคงที่คำนวณจาก Nameplate
sr = (ns - nr) / ns
O = 0.67  # X1/X2
I21Rmin = 0.7
V21Rmin = 0.7
FWL = 1.2 #Define Fiction windage loss
#Define Stray loss
if Pout<93250:
    STL=1.8
elif Pout<186500:
    STL=1.5
elif Pout<186500:
    STL=1.2
else:
    STL=0.9

#Calculation parameter
    
#มีค่า R1
#จุดที่ 1 (A)
Va = 226
Ia = 54.3
PFa = 0.8
na = 1480
r1 = 0.1  # Stator R from measurement
#จุดที่ 2 (B)
Vb = 226
Ib = 54.3
PFb = 0.8
nb = 1480


#ขั้นตอนการคำนวน
#1.คำนวณช่วง MAX-MIN ในการสุ่ม
#r1 = 6.8857 * (float(Pout/1000)** -1.174) # not use if know r1

r2min = (Pout*(1 + FWL/100 + STL/100)*sr)/ (3 * Ir *Ir* (1-sr))
r2max = r2min/(I21Rmin*I21Rmin)

rcmin = 3*(Vr**2)*(V21Rmin**2) / (0.5*(Pout)*((100/effr) -1- FWL/100-STL/100))
rcmax = 3*(Vr**2) / (0.1*(Pout)*((100/effr) -1- FWL/100-STL/100))

Xmmin = (3*(Vr**2) * (V21Rmin**2)) / (3*Vr*Ir*math.sin(math.acos(PFr)))
Xmmax = (3*(Vr**2)) / (3*Vr*Ir*math.sin(math.acos(PFr))*0.2)
X1min = (0.2*3*Vr*Ir*math.sin(math.acos(PFr))) / (3*(Ir**2)*(1+1/O))
X1max = (3*Vr*Ir*math.sin(math.acos(PFr))) / (3*(Ir**2)*(1+(I21Rmin*I21Rmin)/O))
X2min = X1min/O
X2max = X1max/O

print ("motor slip : " ,sr)
print ("R1    : " ,r1)
print("r2min : ",r2min)
print("r2max : ",r2max)
print("rcmin : ",rcmin)
print("rcmax : ",rcmax)
print("Xmmin : ",Xmmin)
print("Xmmax : ",Xmmax)
print("X1min : ",X1min)
print("X1max : ",X1max)

#2.หาค่า Z และ I PF Power จากการคำนวณ(Zin,Ic,PFc,Pc)
# input for loop calculation Point A                                                                                                                                                                                                                                                                                                                           
r1A=r1
r2A=0.072
rcA=420.4669
xmA=9.5622
x1A=0.101
x2A=x1A/O

#find ZA 
sA=(ns-na)/ns
Pa=3*Va*Ia*PFa
NR=(rcA*xmA*r2A/sA)*(rcA*x2A+rcA*xmA+xmA*(r2A/sA))-rcA*x2A*xmA*(rcA*(r2A/sA)-x2A*xmA)
NI=rcA*xmA*(r2A/sA)*(rcA*(r2A/sA)-x2A*xmA)+rcA*x2A*xmA*(rcA*x2A+rcA*xmA+xmA*(r2A/sA))
D=((rcA*(r2A/sA)-x2A*xmA)**2)+((rcA*x2A+rcA*xmA+xmA*(r2A/sA))**2)
ZinRA = r1A+NR/D
ZinIA = x1A+NI/D
ZinA = math.sqrt((ZinRA*ZinRA+ZinIA*ZinIA)) 
#find Ic PFc Pc for Point A
IcA =Va/ZinA
PFcA =ZinRA/ZinA
PcA =3*Va*IcA*PFcA

# input for loop calculation Point B
r1B=r1
r2B=0.072
rcB=420.4669
xmB=9.5622
x1B=0.101
x2B=x1B/O

#find ZB
sB=(ns-nb)/ns
Pb=3*Vb*Ib*PFb
NR=(rcB*xmB*r2B/sB)*(rcB*x2B+rcB*xmB+xmB*(r2B/sB))-rcB*x2B*xmB*(rcB*(r2B/sB)-x2B*xmB)
NI=rcB*xmB*(r2B/sB)*(rcB*(r2B/sB)-x2B*xmB)+rcB*x2B*xmB*(rcB*x2B+rcB*xmB+xmB*(r2B/sB))
D=((rcB*(r2B/sB)-x2B*xmB)**2)+((rcB*x2B+rcB*xmB+xmB*(r2B/sB))**2)
ZinRB = r1B+NR/D
ZinIB = x1B+NI/D
ZinB = math.sqrt((ZinRB*ZinRB+ZinIB*ZinIB)) 
#find Ic PFc Pc for Point B
IcB =Vb/ZinB
PFcB =ZinRB/ZinB
PcB =3*Vb*IcB*PFcB

#3.คำนวณค่า ERROR แต่ละจุด
errA = 100*math.sqrt(((((IcA-Ia)/Ia)**2)+(((PFcA-PFa)/PFa)**2)+(((PcA-Pa)/Pa)**2))/3)
errB = 100*math.sqrt(((((IcB-Ib)/Ib)**2)+(((PFcB-PFb)/PFb)**2)+(((PcB-Pb)/Pb)**2))/3)
errT = math.sqrt(((errA**2)+(errB**2))/2)

print ("errA : " ,errA)
print ("errB : " ,errB)
print ("errT : ",errT)

#4.คำนวนค่าประสิทธิภาพมอเตอร์
#Point A
i1RA=IcA*ZinRA/ZinA
i1IA=-1*IcA*ZinIA/ZinA
v2A2=((Va-(r1A*i1RA-x1A*i1IA))**2)+((-1*(r1A*i1IA+x1A*i1RA))**2)
i2A2=v2A2/(((r2A/sA)**2)+(x2A**2))
effa = 100*i2A2*r2A*(1-sA)/(sA*(1+(FWL/100)+STL*i2A2/(87.75*Ir*Ir))*(Va*IcA*PFcA))

print("effa : ",effa)

#Point B
i1RB=IcB*ZinRB/ZinB
i1IB=-1*IcB*ZinIB/ZinB
v2B2=((Vb-(r1B*i1RB-x1B*i1IB))**2)+((-1*(r1B*i1IB+x1B*i1RB))**2)
i2B2=v2B2/(((r2B/sB)**2)+(x2B**2))
effb = 100*i2B2*r2B*(1-sB)/(sB*(1+(FWL/100)+STL*i2B2/(87.75*Ir*Ir))*(Vb*IcB*PFcB))

print("effb : ",effb)



