import sys
from scipy.optimize import differential_evolution
import math
import json

request_json = json.loads(sys.argv[1])

#ข้อมูลพื้นฐานของมอเตอร์ NAMEPLATE DATA
fac = request_json['fac']
MN = request_json['MN']
Pout = request_json['Pout']
Vr = request_json['Vr']
Ir = request_json['Ir']
PFr = request_json['PFr']
effr = request_json['effr']
ns = request_json['ns']
nr = request_json['nr']

r1 = request_json['r1']
t0 = request_json['t0']
t1 = request_json['t1']

#Input parameter from operation
#จุดที่ 1 (A)
Va = request_json['Va']
Ia = request_json['Ia']
Pa = request_json['Pa']
na = request_json['na']
#จุดที่ 2 (B)
Vb = request_json.get('Vb', None)
Ib = request_json.get('Ib', None)
Pb = request_json.get('Pb', None)
nb = request_json.get('nb', None)

#เพิ่มค่า
Pout = Pout * 1000
Vr = Vr / 1.732
Pa = Pa * 1000
if Pb:
    Pb = Pb * 1000

PFa=Pa/(3*Va*Ia)
if Vb and Ib and Pb and nb:
    PFb=Pb/(3*Vb*Ib)

#define opyimizing function
def zfunc(x, isSingle):
    # input for loop calculation Point A                                                                                                                                                                                                                                                                                                                           
    r1A=a
    x1A=abs(x[0])
    r2A=abs(x[1])
    rcA=abs(x[2])
    xmA=abs(x[3])
    x2A=x1A/O
    #find ZA 
    NR=(rcA*xmA*r2A/sA)*(rcA*x2A+rcA*xmA+xmA*(r2A/sA))-rcA*x2A*xmA*(rcA*(r2A/sA)-x2A*xmA)
    NI=rcA*xmA*(r2A/sA)*(rcA*(r2A/sA)-x2A*xmA)+rcA*x2A*xmA*(rcA*x2A+rcA*xmA+xmA*(r2A/sA))
    D=((rcA*(r2A/sA)-x2A*xmA)**2)+((rcA*x2A+rcA*xmA+xmA*(r2A/sA))**2)
    ZinRA = r1A+NR/D
    ZinIA = x1A+NI/D
    ZinA = math.sqrt((ZinRA*ZinRA+ZinIA*ZinIA)) 

    # input for loop calculation Point B
    r1B=a
    x1B=abs(x[0])
    r2B=abs(x[1])
    rcB=abs(x[2])
    xmB=abs(x[3])
    x2B=x1B/O

    if isSingle:
        return ZinRA,ZinIA,ZinA

    #find ZB
    NR=(rcB*xmB*r2B/sB)*(rcB*x2B+rcB*xmB+xmB*(r2B/sB))-rcB*x2B*xmB*(rcB*(r2B/sB)-x2B*xmB)
    NI=rcB*xmB*(r2B/sB)*(rcB*(r2B/sB)-x2B*xmB)+rcB*x2B*xmB*(rcB*x2B+rcB*xmB+xmB*(r2B/sB))
    D=((rcB*(r2B/sB)-x2B*xmB)**2)+((rcB*x2B+rcB*xmB+xmB*(r2B/sB))**2)
    ZinRB = r1B+NR/D
    ZinIB = x1B+NI/D
    ZinB = math.sqrt((ZinRB*ZinRB+ZinIB*ZinIB))
    return ZinRA,ZinIA,ZinA,ZinRB,ZinIB,ZinB

def func1(x):
    Z=zfunc(x, False)
    ZinRA = Z[0]
    ZinIA = Z[1]
    ZinA = Z[2]
    ZinRB = Z[3]
    ZinIB = Z[4]
    ZinB = Z[5]
    S= (ZRA-ZinRA)**2+(ZIA-ZinIA)**2+(ZRB-ZinRB)**2+(ZIB-ZinIB)**2   
    return S

def func2(x):
    Z=zfunc(x, True)
    ZinRA = Z[0]
    ZinIA = Z[1]
    ZinA = Z[2]
    S= (ZRA-ZinRA)**2+(ZIA-ZinIA)**2
    return S

def func3(x):
    Z=zfunc(x, False)
    ZinRA = Z[0]
    ZinIA = Z[1]
    ZinA = Z[2]
    ZinRB = Z[3]
    ZinIB = Z[4]
    ZinB = Z[5]
    S= (ZRB-ZinRB)**2+(ZIB-ZinIB)**2   
    return S

def efunc(Z, isSingle):
    ZinRA = Z[0]
    ZinIA = Z[1]
    ZinA = Z[2]  
    #Point A
    IcA =Va/ZinA
    PFcA =ZinRA/ZinA
    i1RA=IcA*ZinRA/ZinA
    i1IA=-1*IcA*ZinIA/ZinA
    v2A2=((Va-(r1A*i1RA-x1A*i1IA))**2)+((r1A*i1IA+x1A*i1RA)**2)
    i2A2=v2A2/(((r2A/sA)**2)+(x2A**2))
    #Find i2r for stray loss
    v2r2=((Vr-(r1A*Ir*PFr+x1A*Ir*math.sin(math.acos(PFr))))**2)+((-1*r1A*Ir*math.sin(math.acos(PFr))+x1A*Ir*PFr)**2)
    i2r2=v2r2/(((r2A/sr)**2)+(x2A**2))
    effa = 100*i2A2*r2A*(1-sA)/(sA*(1+(FWL/effr)+STL*i2A2/(100*i2r2))*(Va*IcA*PFcA))
    if isSingle:
        return effa

    ZinRB = Z[3]
    ZinIB = Z[4]
    ZinB = Z[5]  
    #Point B
    IcB =Vb/ZinB
    PFcB =ZinRB/ZinB     
    i1RB=IcB*ZinRB/ZinB
    i1IB=-1*IcB*ZinIB/ZinB
    v2B2=((Vb-(r1B*i1RB-x1B*i1IB))**2)+((-1*(r1B*i1IB+x1B*i1RB))**2)
    i2B2=v2B2/(((r2B/sB)**2)+(x2B**2))
    effb = 100*i2B2*r2B*(1-sB)/(sB*(1+(FWL/effr)+STL*i2B2/(100*i2r2))*(Vb*IcB*PFcB))
    return effa,effb

    
#calculation
############################################
r1=(234.5+t1)*r1/(234.5+t0)
a=r1
#ค่าคงที่คำนวณจาก Nameplate
sr = (ns - nr) / ns
sA=(ns-na)/ns
if Vb and Ib and Pb and nb:
    sB=(ns-nb)/ns
O = 0.67  # X1/X2
I21Rmin = 0.7
V21Rmin = 0.7
#Define Fiction windage loss
if ns >= 3000:
    FWL=2.5
elif ns>=1500:
    FWL=1.2
else :
    FWL=1.0

#Define Stray loss
if Pout<=90000:
    STL=1.8
elif Pout<=375000:
    STL=1.5
elif Pout<=1850000:
    STL=1.2
else:
    STL=0.9
ZRA=Va*PFa/Ia
ZIA=Va*math.sqrt(abs(1-PFa**2))/Ia
if Vb and Ib and Pb and nb:
    ZRB=Vb*PFb/Ib
    ZIB=Vb*math.sqrt(abs(1-PFb**2))/Ib

#ขั้นตอนการคำนวน
#1.คำนวณช่วง MAX-MIN ในการสุ่ม
#r1 = 6.8857 * (float(Pout/1000)** -1.174) # not use if know r1
r1max=0.8*(Pout*(100/effr-1)-Pout*(FWL+STL)/100)/(3*Ir*Ir)
r1min=0.2*r1max
r1mean= 0.4*(Pout*(100/effr-1)-Pout*(FWL+STL)/100)/(3*Ir*Ir)


r2min = 0.95*(Pout*(1 + FWL/100 + STL/100)*sr)/ (3 * Ir *Ir* (1-sr))
r2max = r2min/(I21Rmin*I21Rmin)
r2mean= (Pout*(1 + FWL/100 + STL/100)*sr)/ (3 *0.81* Ir *Ir* (1-sr))

rcmin = 3*(Vr**2)*(V21Rmin**2) / (0.4*(Pout)*((100/effr) -1- FWL/effr-STL/100))
rcmax = 3*(Vr**2) / (0.1*(Pout)*((100/effr) -1- FWL/effr-STL/100))
rcmean = 3*(Vr**2)*0.81 / (0.2*(Pout)*((100/effr) -1- FWL/effr-STL/100))

xmmin = (3*(Vr**2) * (V21Rmin**2)) / (0.9*3*Vr*Ir*math.sin(math.acos(PFr)))
xmmax = (3*(Vr**2)) / (0.1*3*Vr*Ir*math.sin(math.acos(PFr)))
xmmean = (3*(Vr**2))*0.81 / (0.3*3*Vr*Ir*math.sin(math.acos(PFr)))

x1min = (0.1*3*Vr*Ir*math.sin(math.acos(PFr))) / (3*(Ir**2)*(1+1/O))
x1max = (0.9*3*Vr*Ir*math.sin(math.acos(PFr))) / (3*(Ir**2)*(1+(I21Rmin*I21Rmin)/O))
x1mean = (0.3*3*Vr*Ir*math.sin(math.acos(PFr))) / (3*(Ir**2)*(1+0.81/O))
                                       
Bounds=[(x1min,x1max),(r2min,r2max),(rcmin,rcmax),(xmmin,xmmax)]
errT = None

if Vb and Ib and Pb and nb:
    results = dict()
    results = differential_evolution(func1,Bounds)
    X=results.x

    # results of minimize  
    r1A=r1B=a
    x1A=x1B=abs(X[0])
    r2A=r2B=abs(X[1])
    rcA=rcB=abs(X[2])
    xmA=xmB=abs(X[3])
    x2A=x2B=x1A/O

    Z=zfunc(X, False)
    E=efunc(Z, False)
    ZinRA = Z[0]
    ZinIA = Z[1]
    ZinA = Z[2]
    ZinRB = Z[3]
    ZinIB = Z[4]
    ZinB = Z[5]        
    effa=E[0]
    effb=E[1]

    errA = 100*math.sqrt(((((ZinRA-ZRA)/ZRA)**2)+(((ZinIA-ZIA)/ZIA)**2))/2)
    errB = 100*math.sqrt(((((ZinRB-ZRB)/ZRB)**2)+(((ZinIB-ZIB)/ZIB)**2))/2)
    errT = math.sqrt(((errA**2)+(errB**2))/2)

    if errT<1:
        data = {
            'errA': errA,
            'errB': errB,
            'errT': errT,
            'effa': effa,
            'effb': effb
        }
        print(json.dumps(data))
        exit()

#solve for point A only
results = differential_evolution(func2,Bounds)
X=results.x

# results of minimize A 
r1A=r1B=a
x1A=x1B=abs(X[0])
r2A=r2B=abs(X[1])
rcA=rcB=abs(X[2])
xmA=xmB=abs(X[3])
x2A=x2B=x1A/O

Z=zfunc(X, True)
E=efunc(Z, True)
ZinRA = Z[0]
ZinIA = Z[1]
ZinA = Z[2]
effa=E
errA2 = 100*math.sqrt(((((ZinRA-ZRA)/ZRA)**2)+(((ZinIA-ZIA)/ZIA)**2))/2)

data = {
    'errA': errA2,
    'effa': effa
}

if errT:
    data['errT'] = errT

print(json.dumps(data))
exit()
