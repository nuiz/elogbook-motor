const { spawn } = require('child_process');
const http = require('http');
const qs = require('querystring');
const axios = require('axios');
const uuidv4 = require('uuidv4');

const NODE_ENV = process.env.NODE_ENV || 'dev';
const PY_EXEC = process.env.PY_EXEC || 'python';

console.log(`environment: NODE_ENV=${NODE_ENV}`);
console.log(`environment: PY_EXEC=${PY_EXEC}`);

function piper(...fs) {
    return (...args) => fs.reduce((args,f) => [f.apply(this,args)],args)[0];
}

function toDecimal2Point(input) {
    return (Math.round(input * 100) / 100).toFixed(2);
}

function executePython(jsonContent) {
    const process = spawn(PY_EXEC , ['./pythonscript/caleff.py', JSON.stringify(jsonContent)]);
    let successMessage = '';
    let errorMessage = '';
    process.stdout.on('data', function (data) {
        // console.log('stdout data=', data.toString());
        successMessage = data.toString();
    });
    process.stderr.on('data', function (data) {
        // console.log('stderr data=', data.toString());
        errorMessage = data.toString();
    });

    return new Promise(function (resolve, reject) {
        process.addListener("error", () => reject(errorMessage));
        process.addListener("exit", () => resolve(successMessage));
    });
}

const mappings = [
    ["BRAND_MO#", "fac"],
    ["MODEL_MO#", "MN"],
    ["P_RATE_MO#", "Pout"],
    ["V_RATE_MO#", "Vr"],
    ["I_RATE_MO#", "Ir"],
    ["PF_RATE_MO#", "PFr"],
    ["EFF_SPEC_MO#", "effr"],
    ["RPM_SYN_MO#", "ns"],
    ["RPM_RATE_MO#", "nr"],
    ["T0_RATE_MO#", "t0"],
    ["T1_STATOR_MO#", "t1"],
    ["OHM_STATOR_MO#", "r1"],
    ["V_MEA1_MO#", "Va"],
    ["I_MEA1_MO#", "Ia"],
    ["P_MEA1_MO#", "Pa"],
    ["N_MEA1_MO#", "na"],
    ["V_MEA2_MO#", "Vb"],
    ["I_MEA2_MO#", "Ib"],
    ["P_MEA2_MO#", "Pb"],
    ["N_MEA2_MO#", "nb"],
];

const allowOriginHeaders = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Method": "*",
    "Access-Control-Allow-Headers": "*"
};
const server = http.createServer(function(req, res) {

    const [path, plainQueryString] = req.url.split('?');
    const traceId = uuidv4.uuid();
    console.info(`traceId=${traceId}: received request url=${req.url}, , method=${req.method}`);

    if (req.method == 'OPTIONS') {
        res.writeHead(200, allowOriginHeaders);
        res.end();
        return;
    }
    if (['', '/'].includes(path) && req.method == 'GET') {
        executeCalAndLog(traceId, req, res);
    }
    else if (['cal-web', '/cal-web'].includes(path) && req.method == 'POST') {
        executeCalAndReturnWeb(traceId, req, res);
    }
    else {
        console.info(`traceId=${traceId}: not found resolver request url=${req.url}, method=${req.method}`);
        res.writeHead(404);
        res.end();
    }
});

// ALL /
function executeCalAndLog(traceId, req, res) {
    res.writeHead(200);
    res.end();

    const [path, plainQueryString] = req.url.split('?');

    const parsedQuery = qs.parse(plainQueryString);
    const deviceId = Object.keys(parsedQuery)
        .find((value) => /^P_RATE_MO\d+$/.test(value))
        .replace('P_RATE_MO', '');

    console.info(`traceId=${traceId}: parsed request deviceId=${deviceId}`);

    Promise.resolve()
        .then(() => mappings.reduce((acc, el) => {
            const key = el[1];
            const value = piper(
                (value) => value.replace('#', deviceId),
                (value) => parsedQuery[value],
                (value) => /^-?\d+\.?\d*$/.test(value) ? new Number(value) : value,
            )(el[0]);
            return {...acc, [key]: value};
        }, {}))
        .then((mappedInput) => console.log(`traceId=${traceId}: mappedInput=${JSON.stringify(mappedInput)}`) || mappedInput)
        .then((mappedInput) => executePython(mappedInput)
            .then((data) => console.info(`traceId=${traceId}: calculated resultData=${data}`) || data)
            .then(JSON.parse)
        )
        .then((resultData) => {
            const logRequestContent = {'__company__': parsedQuery.company};
            const resultDataKeys = Object.keys(resultData);
            if (resultDataKeys.includes('effa')) {
                logRequestContent[`EFF_A_MO${deviceId}`] = toDecimal2Point(resultData.effa);
            }
            if (resultDataKeys.includes('effb')) {
                logRequestContent[`EFF_B_MO${deviceId}`] = toDecimal2Point(resultData.effb);
            }
            if (resultDataKeys.includes('errA')) {
                logRequestContent[`ERR_A_MO${deviceId}`] = toDecimal2Point(resultData.errA);
            }
            if (resultDataKeys.includes('errB')) {
                logRequestContent[`ERR_B_MO${deviceId}`] = toDecimal2Point(resultData.errB);
            }
            if (resultDataKeys.includes('errT')) {
                logRequestContent[`ERR_T_MO${deviceId}`] = toDecimal2Point(resultData.errT);
            }
            logRequestContent['datetime'] = parsedQuery.datetime;

            console.info(`traceId=${traceId}: logRequestContent=${JSON.stringify(logRequestContent)}`)

            return logRequestContent;
        })
        .then((logRequestContent) => axios.get(`http://103.91.189.148/elogbook/log.php?${qs.stringify(logRequestContent)}`)
            .then((logResponse) => console.info(`traceId=${traceId}: response.status=${logResponse.status}, response.body=${logResponse.data}`))
        )
        .catch((err) => {
            console.error(`traceId=${traceId}:`, err);
            res.writeHead(500, {...allowOriginHeaders, "Content-Type": "application/json"});
            res.end(JSON.stringify(err));
        });
}

// ALL /cal-web
function executeCalAndReturnWeb(traceId, req, res) {
    let body = [];
    req.on('data', chunk => {
        console.log(chunk.toString());
        body.push(chunk);
    }).on('end', () => {
        Promise.resolve(Buffer.concat(body).toString())
            .then((body) => console.log(`traceId=${traceId}: body=${body}`) || body)
            .then((body) => JSON.parse(body))
            .then((jsonInput) => executePython(jsonInput)
                .then((plainJsonOutput) => console.info(`traceId=${traceId}: calculated plainJsonOutput=${plainJsonOutput}`) || plainJsonOutput)
                .then((plainJsonOutput) => {
                    res.writeHead(200, {...allowOriginHeaders, "Content-Type": "application/json"});
                    res.end(plainJsonOutput);
                })
            )
            .catch((err) => {
                console.error(`traceId=${traceId}:`, err);
                res.writeHead(500, {...allowOriginHeaders, "Content-Type": "application/json"});
                res.end(JSON.stringify(err));
            });
    });
}

server.listen(8010, () => {
    console.info('listen with 8010');
    // ### local testing ###
    // const reqUrl = 'http://localhost:8010?company=202003281513Hxf&user=sensor&datetime=200604075037&OHM_STATOR_MO4001=0.04&T1_STATOR_MO4001=25&V_MEA1_MO4001=228&I_MEA1_MO4001=105&P_MEA1_MO4001=78000&N_MEA1_MO4001=1470&T_MEA1_MO4001=67&V_MEA2_MO4001=225&I_MEA2_MO4001=102&P_MEA2_MO4001=77000&N_MEA2_MO4001=1440&T_MEA2_MO4001=551666&BRAND_MO4001=VEM&MODEL_MO4001=VDT0530T1&P_RATE_MO4001=90&V_RATE_MO4001=380&I_RATE_MO4001=168&PF_RATE_MO4001=0.86&EFF_SPEC_MO4001=93.41&RPM_SYN_MO4001=1500&RPM_RATE_MO4001=1480&T0_RATE_MO4001=25';
    // axios.get(reqUrl)
    //     .then((req) => req.data)
    //     .then(console.log);
});
